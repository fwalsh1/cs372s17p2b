package edu.luc.cs.cs372.expressionsAlgebraic

import matryoshka.Algebra
import structures.shapeFactory._
import matryoshka.data.Fix


/**
 * In this example, we define several behaviors of arithmetic expressions
 * as specific ExprF-algebras. Note the nonrecursive nature of these
 * algebras.
 */

object behaviors {

  import structures._

  // TODO parsing as unfold (prefix or postfix notation)
  // TODO unparsing/printing


  /** Evaluates an expression to a value. */
  /*val evaluate: Algebra[ShapeF, Location] = {
    case Constant(c) => c
    case UMinus(r)   => -r
    case Plus(l, r)  => l + r
    case Minus(l, r) => l - r
    case Times(l, r) => l * r
    case Div(l, r)   => l / r
    case Mod(l, r)   => l % r
  }*/
  //algebra maps shape one collection to another collection
  val boundingBox: Algebra[ShapeF, Location[Rectangle]] = {
    // Location(0, 0, s)
    case Rectangle(w, h) => Location(0, 0, Rectangle(w, h))
    case Ellipse(min, maj) => Location(0 - min, 0 - maj, Rectangle(2 * min, 2 * maj))
    case Location(x, y, s) =>

      Location(x + s.x,y + s.y, s.shape)
      //needs to return a rectangle with a


    case Group(s) =>
     // val bBoxes = shapes.map(s => boundingBox(s))
      val minX = s.map(b => b.x).min
      val maxX = s.map(b => b.x  + b.shape.width).max
      val minY = s.map(b => b.y).min
      val maxY = s.map(b => b.y  + b.shape.height).max
      //locationsList foldLeft

      ///recursively
      Location(minX, minY, Rectangle(maxX - minX, maxY - minY))


    case _ => Location(0, 0, Rectangle(0, 0))

  }


  /** Computes the number of nodes in an expression. */
  val size: Algebra[ShapeF, Int] = {

    case Ellipse(min, maj) => 1
    case Rectangle(_, _) => 1
    case Location(x, y, s) => s
    case Group(s) => s.sum


  }

  val height: Algebra[ShapeF, Int] = {
    case Ellipse(min, maj) => 1
    case Rectangle(w, h) => 1
    case Location(x, y, s) => 1 + s
    case Group(s) => 1 + s.max

  }

  def scale(factor: Int): Algebra[ShapeF, Shape] = {

    case Ellipse(min, maj) => ellipse(min*factor, maj*factor)
    case Rectangle(h, w) => rectangle(h*factor, w*factor)
    case Location(x, y, s) => location(x*factor, y*factor, s)
    case Group(List(scaledShapes)) => group(scaledShapes)

  }



    //        case UMinus(r)   => 1 + r
    //        case Plus(l, r)  => 1 + math.max(l, r)
    //        case Minus(l, r) => 1 + math.max(l, r)
    //        case Times(l, r) => 1 + math.max(l, r)
    //        case Div(l, r)   => 1 + math.max(l, r)
    //        case Mod(l, r)   => 1 + math.max(l, r)
    //      }

    //    case UMinus(r)   => 1 + r
    //    case Plus(l, r)  => 1 + l + r
    //    case Minus(l, r) => 1 + l + r
    //    case Times(l, r) => 1 + l + r
    //    case Div(l, r)   => 1 + l + r
    //    case Mod(l, r)   => 1 + l + r
    //  }

    /** Computes the height of an expression tree. */
    //

    /**
      * Evaluates an expression representing a natural number.
      * If any of the partial results becomes negative,
      * evaluation fails.
      */
    //  val evaluateNat: Algebra[ExprF, Option[Int]] = {
    //    case Constant(c) => for { v <- Some(c); if v >= 0 } yield v
    //    case UMinus(r)   => None
    //    case Plus(l, r)  => for { l1 <- l; r1 <- r } yield l1 + r1
    //    case Minus(l, r) => for { l1 <- l; r1 <- r; if l1 >= r1 } yield l1 - r1
    //    case Times(l, r) => for { l1 <- l; r1 <- r } yield l1 * r1
    //    case Div(l, r)   => for { l1 <- l; r1 <- r } yield l1 / r1
    //    case Mod(l, r)   => for { l1 <- l; r1 <- r } yield l1 % r1
    //  }*/


}