package edu.luc.cs.cs372.expressionsAlgebraic
import structures.shapeFactory._
object fixtures {

  // TODO comment these fixtures back in after adding the required case classes
  val simpleEllipse = ellipse(50, 30)

  val simpleRectangle = rectangle(80, 120)

  val simpleLocation = location(70, 30, rectangle(80, 120))

  val ellipseByTwo = ellipse(100, 60)


  val simpleRectangleByTwo = rectangle(160, 240)

  val simpleLocationByTwo = location(140, 60, rectangle(160, 240))


  val basicGroup = group(
    ellipse(50, 30),
    rectangle(20, 40))

    val basicGroupByTwo = group(ellipse(100,60),rectangle(40,80))


    val simpleGroup = group(
      location(200, 100, ellipse(50, 30)),
      location(400, 300, rectangle(100, 50))
    )

  val simpleGroupByThree = group(
    location(600, 300, ellipse(150, 90)),
    location(1200, 900, rectangle(300, 150))
  )

    val complexGroup =
      location(50, 100,
        group(
          ellipse(20, 40),
          location(150, 50,
            group(
              rectangle(50, 30),
              rectangle(300, 60),
              location(100, 200,
                ellipse(50, 30)
              )
            )),
          rectangle(100, 200)
        ))

  val complexGroupByThree =
    location(150, 300,
      group(
        ellipse(60, 120),
        location(450, 150,
          group(
            rectangle(150, 90),
            rectangle(900, 180),
            location(300, 600,
              ellipse(150, 90)
            )
          )),
        rectangle(300, 600)
      ))
  }


  //object fixtures {
  //  val simple1 = plus(constant(1), constant(2))
  //
  //  val simple2 = minus(constant(3), constant(2))
  //
  //  val complex1 =
  //    div(
  //      minus(
  //        plus(
  //          constant(1),
  //          constant(2)
  //        ),
  //        times(
  //          constant(3),
  //          constant(4)
  //        )
  //      ),
  //      constant(5)
  //    )
  //
  //  val complex1string = "((1 + 2) - (3 * 4)) / 5"
  //
  //  val complex2 =
  //    mod(
  //      minus(
  //        plus(
  //          constant(1),
  //          constant(2)
  //        ),
  //        times(
  //          uminus(
  //            constant(3)
  //          ),
  //          constant(4)
  //        )
  //      ),
  //      constant(5)
  //    )

