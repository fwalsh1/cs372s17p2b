package edu.luc.cs.cs372.expressionsAlgebraic

import org.scalatest.FunSuite

class behaviorTests extends FunSuite {
    import matryoshka.implicits._
    import fixtures._
    import structures._
    import behaviors._

    def testBoundingBox(description: String, s:Shape, x: Int, y: Int, width: Int, height: Int) = {
      test(description) {
        val b = s cata boundingBox
        //val r = b.shape.asInstanceOf[Rectangle]
        //      val Location(u, v, Rectangle(w, h)) = boundingBox(s)
        assert(x === b.x)
        assert(y === b.y)
        assert(width === b.shape.width)
        assert(height === b.shape.height)

      }
    }



    def testSize(description: String, s: Shape, retSize: Int) = {
      test(description) {

        val n = s cata size
        assert(n === retSize)


      }
    }


    def testHeight(description: String, s: Shape, retHeight: Int) = {
            test(description)  {
            val h = s cata height
            assert(h=== retHeight)
          }
    }

    def testScale(description: String, s: Shape, scaleFactor: Int, resultShape: Shape) = {
      test(description)  {
        val h = s cata scale(scaleFactor)
        assert(h === resultShape)
      }
    }


    testBoundingBox("simple ellipse", simpleEllipse, -50, -30, 100, 60)
    testBoundingBox("simple rectangle", simpleRectangle, 0, 0, 80, 120) //orig
    testBoundingBox("simple location", simpleLocation, 70, 30, 80, 120) //orig
    testBoundingBox("basic group", basicGroup, -50, -30, 100, 70)
    testBoundingBox("simple group", simpleGroup, 150, 70, 350, 280)
    testBoundingBox("complex group", complexGroup, 30, 60, 470, 320)



    testSize("simple ellipse size",simpleEllipse,1)
    testSize("simple rectangle size",simpleRectangle,1)
    testSize("simple location size",simpleLocation,1)
    testSize("basic group size", basicGroup, 2)
    testSize("simple group size",simpleGroup,2)
    testSize("complex group size",complexGroup,5)


    testHeight("simple ellipse height",simpleEllipse,1)
    testHeight("simple rectangle height",simpleRectangle,1)
    testHeight("simple location height",simpleLocation,2)
    testHeight("basic group height", basicGroup, 2)
    testHeight("simple group height",simpleGroup,3)
    testHeight("complex group height",complexGroup,6)

    testScale("simple ellipse scale",simpleEllipse, 2, ellipseByTwo)
    testScale("simple rectangle scale",simpleRectangle, 2, simpleRectangleByTwo)
    testScale("simple location scale",simpleLocation,2, simpleLocationByTwo)
    testScale("basic group scale", basicGroup, 2, basicGroupByTwo)
    testScale("simple group scale",simpleGroup,3, simpleGroupByThree)
    testScale("complex group scale",complexGroup,3, complexGroupByThree)
   // testHeight("

//    def testScale(description: String,s: Shape,factor:Int, expectedResultShape: Shape) = {
//      test(description) {
//
//        val scaledShape = behaviours.scale(s,factor)
//
//
//        assert(scaledShape === expectedResultShape)
//
//      }
//
//    }


//
//


//    testScale("simple ellipse scaled",simpleEllipse,2,ellipseByTwo)
//    testScale("simple rectangle scaled",simpleRectangle,2,simpleRectangleByTwo)
//    testScale("simple location scaled",simpleLocation,2,simpleLocationByTwo)
//   // testScale("simple group scaled",basicGroup,2,basicGroupByTwo)
    //add test cases from scale

    // DONE comment these tests back in
    //bounding box



    //Size
//
////
//
//
//    testHeight("basic group height", basicGroup, 2)
//    testHeight("simple group height",simpleGroup,3)
//    testHeight("complex group height",complexGroup,6)





    //

//  def testBoundingBox(description: String, s:Shape, x: Int, y: Int, width: Int, height: Int) = {
//    test(description) {
//      val b = behaviours.boundingBox(s)
//      val r = b.shape.asInstanceOf[Rectangle]
//      //      val Location(u, v, Rectangle(w, h)) = boundingBox(s)
//      assert(x === b.x)
//      assert(y === b.y)
//      assert(width === r.width)
//      assert(height === r.height)
//    }
//  }

//    def testBoundingBox()
//  property("boundingBox1") = Prop { (fixtures.complex1 cata boundingBox) == -1 }
//  property("evaluate2") = Prop { (fixtures.complex2 cata evaluate) == 0 }


//  property("size1") = Prop { (fixtures.complex1 cata size) == 9 }
//  property("size2") = Prop { (fixtures.complex2 cata size) == 10 }
//
//  property("height1") = Prop { (fixtures.complex1 cata height) == 4 }
//  property("height2") = Prop { (fixtures.complex2 cata height) == 5 }
//
//  property("evaluateNat1") = Prop { (fixtures.simple1 cata evaluateNat) == Some(3) }
//  property("evaluateNat2") = Prop { (fixtures.simple2 cata evaluateNat) == Some(1) }
//  property("evaluateNat3") = Prop { (fixtures.complex1 cata evaluateNat) == None }
//  property("evaluateNat4") = Prop { (fixtures.complex2 cata evaluateNat) == None }
}